ansible-rpi-k8s
=================

Installe Kubernetes sur Raspberry Pi

Lancement du rôle Ansible testé depuis un poste linux

Pré-requis
------------

  - Ansible 2.6 (peut fonctionner avec version inférieure, non testé, ne fonctionne pas correctement en 2.7)
  - Raspbian installé sur un ou plusieurs Raspberry Pi
  - SSH activé sur le Raspberry Pi
  - Docker installé sur les cibles

Variables du rôle
------------------
  * k8s_master: true si le noeud est master
  * k8s_version: version de Kubernetes (testée 1.13, 1.14)
  * k8s_master_token: token kubernetes (de la forme "abcdef.0123456789abcdef")
  * k8s_cluster_group: groupe de l'inventaire associé au cluster (le 1er serveur du groupe sera maître)
  * k8s_kubeconfig_save_path: chemin de sauvegarde du fichier de configuration kubernetes

Exemple de playbook
----------------
    # Déploiement sur un Rapsberry Pi
    - name: "Installation de k8s sur le Raspberry Pi"
      hosts: rpi-k8s
      remote_user: "pi"
      roles:
          - ../../roles/ansible-raspberrypi-k8s
      vars:
        k8s_version: "1.14.0-00"
        k8s_master_token: "abcdef.0123456789abcdef"
        k8s_cluster_group: rpi-k8s


Exemple d'inventaire
---------------------

    [rpi1]
    192.168.1.190 servername=rpi1 k8s_master=true

    [rpi2]
    192.168.1.191 servername=rpi2

    [rpi-k8s:children]
    rpi1
    rpi2

Exemple de lancement (via clef ssh, sans mot de passe via une clef ssh)
--------------------------------------------------
    ansible-playbook -b -i inventory playbook.yml
    # -b, --become        joue le rôle ansible en utilisateur avec privilège (root)
    # -i INVENTORY        fichier d'inventaire

Licence
-------

GNU GENERAL PUBLIC LICENSE v3

Information sur l'auteur
-------------------------

Julien LS - contact@jls42.org  
Site Web : https://jls42.org
